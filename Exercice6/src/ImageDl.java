import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


public class ImageDl {
    public static void main(String[] args) throws IOException {

        System.out.println("Entrez l'url de l'image");
        System.out.println("N'oubliez pas de rentrer un espace suite à l'url pour ne pas juste ouvrir l'image");
        System.out.println("URL Type Conseillé : http://i0.kym-cdn.com/photos/images/facebook/000/011/296/success_baby.jpg");
        //scanner de l'entrée utilisateur de l'url
        Scanner scan = new Scanner(System.in);
        String chosenUrl = scan.nextLine();

        try {
            //constructeur de l'url en fonction de l'entrée utilisateur
            URL url = new URL(chosenUrl);
            //mise en mémoire tampon du contenu de l'url
            BufferedImage img =  ImageIO.read(url);
            //création d'un nouveau fichier à partir du Buffer
            ImageIO.write(img, "jpg", new File("./Exercice6/Content/freshNewImage.jpg"));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
