import java.io.File;
import java.util.Scanner;

public class ListFiles {
    public static void main(String[] args) {

        //création du scanner qui va permettre de lire l'entrée utilisateur du chemin
        System.out.println("Entrez le chemin");
        Scanner scanPath = new Scanner(System.in);
        String chosenPath = scanPath.nextLine();

        File path = new File(chosenPath);
        //création du tableau contenant la liste
        File[] fileList = path.listFiles();

        //si le chemin mène à un dossier...
        if (path.exists() && path.isDirectory()){
            //...retourner la liste du contenu quand il s'agit de fichiers
            for (File file : fileList){
                if (file.isFile()){
                    System.out.println(file.getName());
                }
            }
        }
        else{System.out.println("Ce chemin d'accès n'existe pas");}

    }
}
