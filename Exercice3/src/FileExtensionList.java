import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Scanner;

public class FileExtensionList {
    public static void main(String[] args) {

        //création du scanner qui va permettre de lire l'entrée utilisateur du chemin
        System.out.println("Entrez le chemin");
        Scanner scanPath = new Scanner(System.in);
        String chosenPath = scanPath.nextLine();

        File path = new File(chosenPath);

        if (path.exists() && path.isDirectory()){
            //création du scanner lisant l'extension choisie
            System.out.println("Entrez l'extension voulue");
            String chosenExt = scanPath.nextLine();

            //File filter est une fonction qui va envoyer chaques fichiers trouvés dans la fonction accept
            // et en fonction des conditions elle va les ajouter au tableau fileList ou non
            File[] fileList = path.listFiles(new FilenameFilter(){
                @Override
                public boolean accept(File pathname, String name) {
                    return name.endsWith(chosenExt);
                }
            });
            //affichage des fichiers ajoutés au tableau
            for (File file : fileList){
                System.out.println(file.getName());
            }
        }

    }
}
