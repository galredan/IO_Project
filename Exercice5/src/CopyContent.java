import java.io.*;

public class CopyContent {
    public static void main(String[] args) throws IOException {

        //définition des fichiers à utiliser ou création de ces derniers s'ils n'existent pas dans la directory
        File file1 = new File("./Exercice5/Content/fileToCopy.txt");
            if(!file1.exists()) {
                file1.createNewFile();
            }

        File file2 = new File("./Exercice5/Content/fileToPaste.txt");
            if(!file2.exists()) {
                file2.createNewFile();
            }
        //création du reader pour lire le contenu du fichier1
        FileReader read = new FileReader(file1);

        try(
                /*création du BufferedReader à partir du Reader, car il est plus rapide, et est efficace pour lire les
                caractères, les tableaux et les lignes */
                BufferedReader reader = new BufferedReader(read);
        ) {
            try (
                    //création du writer pour écrire dans le fichier2
                    FileWriter writer = new FileWriter(file2)
            ) {
                //tant que le reader reçoit des informations...
                while (reader.ready()) {
                    //...écrire dans le fichier2 en concaténant un retour à la ligne lorsque nécéssaire
                    writer.write(reader.readLine()+"\n");
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
