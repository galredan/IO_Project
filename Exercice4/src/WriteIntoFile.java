import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteIntoFile {
    public static void main(String[] args) throws IOException {

        //création du scanner qui va permettre de lire où créer le fichier texte
        System.out.println("Entrez le chemin où créer le fichier texte");
        Scanner scanPath = new Scanner(System.in);
        String chosenPath = scanPath.nextLine();
        //création du fichier à l'endroit choisit
        File freshNewFile = new File(chosenPath);

        String line = "";
        Scanner lineRead = new Scanner(System.in);

        //écriture dans le fichier
        try (FileWriter writer = new FileWriter(freshNewFile)) {
            //lancer la boucle tant que "quit" n'est pas entré
            while (!line.equals("quit")) {
                //récupération de la saisie
                line = lineRead.nextLine();
                if (!line.equals("quit")) {
                    //écriture de la saisie + retour à la ligne
                    line = line + "\n";
                    writer.write(line);
                }
                else{
                    writer.close();
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}