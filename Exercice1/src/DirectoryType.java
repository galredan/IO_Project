import java.io.File;
import java.util.Scanner;

public class DirectoryType {
    public static void main(String[] args){

        //création du scanner qui va permettre de lire l'entrée utilisateur du chemin
        System.out.println("Entrez le chemin");
        Scanner scanPath = new Scanner(System.in);
        String chosenPath = scanPath.nextLine();

        File path = new File(chosenPath);
        //test d'existence du fichier/répertoire
        if (path.exists()){
            //retour à l'utilisateur du type
            if (path.isDirectory()) {
                System.out.println("Le chemin donne accès au répertoire " + '"' + path.getAbsolutePath() + "\"");
            }
            //"else if" car je ne sais pas s'il existe des types autres que Directory ou File
            else if (path.isFile()){
                System.out.println("Le chemin donne accès au fichier " + path.getName());
            }
        }
        else {
            System.out.println("Ce chemin d'accès ne mène à rien");
        }

    }
}
